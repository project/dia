<?php

// Drupal profile.module category the profile fields are in
function dia_get_category() {
  return 'Personal Information';
}

function dia_get_host() {
  return "https://secure.democracyinaction.org";
}

function dia_get_key() {
  return '0000';
}

function dia_get_login() {
  return 'user@example.com';
}

function dia_get_pass() {
  return 'password';
}

/**
General DIA Field mapping layout

Email address - Email
First Name - First_Name
Last Name - Last_Name
Phone Number - Phone
Street Address - Street
City - City
State - State
Zip Code - Zip
Birthdate - DATETIME1YEAR-DATETIME1MONTH-DATETIME1DAY

Homepage - Web_Page
AIM Account - Instant_Messenger_Name (Also set IM provider to AIM: Instant_Messenger_Service - AOL)
Volunteer - (New custom field)
Mailing List Subscriptions - Receive_Email
Member since: - ?
Profile - (New custom field)
Interests - (New custom field)
Occupation - Occupation

Username - Drupal_Username
Drupal User Id - uid
Module ID - Source
IP & URL - Source_Details
**/

/**
 * dia_field => array('field' => drupal_profile_field, , 'type' => 'text');
 * type should equal 'date' if using the drupal 'date' profile field type
 * only include drupal profile fields here (no email, user id, etc)
 **/
function dia_get_fieldmap() {
  return array(
    'First_Name' => array('field' => 'profile_first_name', 'type' => 'text'),
    'Last_Name' => array('field' => 'profile_last_name', 'type' => 'text'),
    'Zip' => array('field' => 'profile_zip', 'type' => 'text'),
    'Street' => array('field' => 'profile_street', 'type' => 'text'),
    'City' => array('field' => 'profile_city', 'type' => 'text'),
    'Phone' => array('field' => 'profile_phone', 'type' => 'text'),
    'State' => array('field' => 'profile_state', 'type' => 'text'),
    'Web_Page' => array('field' => 'profile_homepage', 'type' => 'text'),
    'Instant_Messenger_Name' => array('field' => 'profile_aim', 'type' => 'text'),
    'DATETIME1' => array('field' => 'profile_bday', 'type' => 'date', 'copa' => FALSE)
  );
}
